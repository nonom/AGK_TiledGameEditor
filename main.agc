// Project: TileEditor2 
// Created: 20-07-20

// show all errors

SetErrorMode(2)

//#option_explicit

#include "./pixeleditor.agc"

local data as integer [256] = [
   0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0
   0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0
   0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0
   0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0
   0,0,1,1,0,1,1,0,0,1,1,0,1,1,0,0
   0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0
   0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0
   0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0
   0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0
   0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0
   0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0
   0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0
   0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0
   0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0
   0,0,1,1,1,0,0,0,0,0,0,1,1,0,0,0
   0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0
]
           
SetWindowSize( 1024, 768, 0 )
SetWindowAllowResize( 1 ) // allow the user to resize the window
SetVirtualResolution( 1024, 768 ) // doesn't have to match the window
SetDisplayAspect( 1024.0 / 768.0 )
SetSyncRate( 30, 0 ) // 30fps instead of 60 to save battery
SetScissor( 0,0,0,0 ) // use the maximum available screen space, no black borders
UseNewDefaultFonts( 1 )

PixelEditor_Init(200, 200)
do
	PixelEditor_Update()
    Sync()
loop
