//***************************************
//*** ColorPicker functions ***
//***************************************

type tColorPicker
	spr as integer
	red as integer
	green as integer
	blue as integer
	exists as integer
endtype

global ColorPicker as tColorPicker

//*** Creates a sprite for the ColorPicker ***
function CreateColorPicker(x as float, y as float, w as float, h as float, img as string)
	global spr as integer
	spr = CreateSprite(0)
	SetSpritePosition(spr, 40, 310)
	SetSpriteSize(spr, 50, 50)
	//*** Create sprite ***
	if img = ""
		sprID = CreateSprite(BuildDefaultColorPicker())
	else
		sprID = CreateSprite(LoadImage(img))
	endif
	SetSpriteSize(sprID,w,h)
	SetSpritePosition(sprID,x,y)
	//*** Store sprite ID ***
	ColorPicker.spr = sprID
	//*** Set selected colour to black ***
	ColorPicker.red = 0
	ColorPicker.green = 0
	ColorPicker.blue = 0
	//*** Mark as exists ***
	ColorPicker.exists = 1
endfunction

//*** Returns 1 if a ColorPicker exists ***
function GetColorPickerExists()
	result = ColorPicker.exists
endfunction result

//*** Returns colour under mouse when clicked ***
function ColorPicker_update()
	result = -1
	if NOT GetColorPickerExists()
		exitfunction result
	endif
	//*** If clicked on colour picker ...
	if GetPointerPressed() and GetSpriteHit(GetPointerX(), GetPointerY()) = ColorPicker.spr
		Render()
		//*** Capture this part of screen as image ***
		imgID = GetImage(GetPointerX(), GetPointerY(), 1,1)
		//*** Convert to a memblock ***
		memID = CreateMemblockFromImage(imgID)
		//*** Get colour of first pixel’s data ***
		ColorPicker.red = GetMemblockByte(memID,12)
		ColorPicker.green = GetMemblockByte(memID,13)
		ColorPicker.blue = GetMemblockByte(memID,14)
		//*** Create a single number from colours ***
		result = 0xFF000000+(ColorPicker.blue<<16) + (ColorPicker.green<<8) + ColorPicker.red
		//*** Delete the memblock and image ***
		DeleteMemblock(memID)
		DeleteImage(imgID)
	endif
	if GetColorPickerExists()
		SetSpriteColor(spr,GetColorPickerRed(), GetColorPickerGreen(), GetColorPickerBlue(), 255)
		PixelEditor.currentColor = MakeColor(GetColorPickerRed(), GetColorPickerGreen(), GetColorPickerBlue())
	endif
endfunction result

//*** Returns the red value of last selection ***
function GetColorPickerRed()
	if NOT GetColorPickerExists()
		exitfunction 0
	endif
	result = ColorPicker.red
endfunction result

//*** Returns the green value of last selection ***
function GetColorPickerGreen()
	if NOT GetColorPickerExists()
		exitfunction 0
	endif
	result = ColorPicker.green
endfunction result

//*** Returns the blue value of last selection ***
function GetColorPickerBlue()
	if NOT GetColorPickerExists()
		exitfunction 0
	endif
	result = ColorPicker.blue
endfunction result

//*** Sets layer of ColorPicker sprite ***
function SetColorPickerDepth(layer as integer)
	if NOT GetColorPickerExists() OR layer < 0 OR layer > 10000
		exitfunction
	endif
	//*** Set sprite depth ***
	SetSpriteDepth(ColorPicker.spr,layer)
endfunction

//*** Returns layer of ColorPicker sprite ***
function GetColorPickerDepth(layer as integer)
	if NOT GetColorPickerExists()
		exitfunction -1
	endif
	result = GetSpriteDepth(ColorPicker.spr)
endfunction result

//*** Deletes ColorPicker widget ***
function DeleteColorPicker()
	if NOT GetColorPickerExists()
		exitfunction
	endif
	DeleteImage(GetSpriteImageID(ColorPicker.spr))
	DeleteSprite(ColorPicker.spr)
	ColorPicker.exists = 0
endfunction

//**************************************
//*** Helper functions ***
//**************************************
//*** Creates a default image for ColorPicker ***
//*** Used when no image supplied ***
function BuildDefaultColorPicker()
	ClearScreen()
	DrawBox(0,0,20,20,0xFF0000FF, 0xFF00FF00,0xFFFF0000,0xFF000000,1)
	Render()
	result = GetImage(0,0,20,20)
	ClearScreen()
endfunction result
