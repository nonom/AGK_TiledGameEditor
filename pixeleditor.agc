#include "./colorpicker.agc"

// Types
type PixelEditor_tColor
	r as integer
	g as integer
	b as integer
endtype

type PixelEditor_tPixelEditorTile
	color as integer
	filled as integer
endtype

type PixelEditor_tColorPalette
	color as PixelEditor_tColor[]
endtype


type PixelEditor_tPixelEditor
	grid as PixelEditor_tPixelEditorTile[16,16]
	palette as PixelEditor_tColorPalette
	currentColor as integer
	saved as integer
endtype

// Globals
global PixelEditor_x, PixelEditor_y, PixelEditor_tx, PixelEditor_ty as integer
global PixelEditor as PixelEditor_tPixelEditor

/**
 * PixelEditor Functions
 */
function PixelEditor_Init(x as integer, y as integer)
	PixelEditor.currentColor = MakeColor(255,255,255)
	local i as integer
	local j as integer
	for i = 0 to 15 step 1
		for j = 0 to 15 step 1
			PixelEditor.grid[i,j].color = PixelEditor.currentColor
			PixelEditor.grid[i,j].filled = 0
		next j
	next i
	CreateColorPicker(280, 20, 200, 200, "")
endfunction

function PixelEditor_Update()
	ColorPicker_Update()
	local i as integer
	local j as integer
	for i = 0 to 15 step 1
		for j = 0 to 15 step 1
			local x1 as integer
			local y1 as integer
			local x2 as integer
			local y2 as integer
			local r as integer
			local g as integer
			local b as integer
			local color as integer
			x1 = i * 16
			y1 = j * 16
			x2 = 16 * i + 16
			y2 = 16 * j + 16
			r = GetColorRed(PixelEditor.grid[i, j].color)
			g = GetColorGreen(PixelEditor.grid[i, j].color)
			b = GetColorBlue(PixelEditor.grid[i, j].color)
			color =  0xFF000000+(b<<16) + (g<<8) + r
			DrawBox(x1, y1, x2, y2, color, color, color, color, PixelEditor.grid[i, j].filled)
		next j
	next i
	if GetPointerState() = 1
		local x as integer
		local y as integer
		local tx as integer
		local ty as integer
		x = GetPointerX()
		y = GetPointerY()
		tx = (x * 16) / 256
		ty = (y * 16) / 256
		if tx > 16 or tx < 0 then exitfunction
		if ty > 16 or ty < 0 then exitfunction
		PixelEditor.grid[tx, ty].color = PixelEditor.currentColor
		PixelEditor.grid[tx, ty].filled = 1
	endif
endfunction